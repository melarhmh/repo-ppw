$(document).ready(function() {
	$("#changemood").click(function(){
		if($("body").css("background-color") == "rgb(175, 203, 255)") {
			$("body").css("background-color", "#13274A");
			$(".accordion").css("background-color", "#13274A");
			$(".accordion").css("color", "#F9FBF2");
			$(".panel-profile").css("color", "#3D348B");
			$("#changemood").removeClass("btn-light");
			$("#changemood").addClass("btn-dark");

		} else {
			$("body").css("background-color", "#AFCBFF");
			$(".accordion").css("background-color", "#AFCBFF");
			$(".accordion").css("color", "#0E1C36");
			$(".panel-profile").css("color", "#13274A");
			$("#changemood").removeClass("btn-dark");
			$("#changemood").addClass("btn-light");
		}
	}); 

	
	var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
  }
});