$(document).ready(function() {
	$("#daymode").click(function(){		
		$("body").css("background-color", "#AFCBFF");
		$(".accordion").css("background-color", "#E2ECFF");
		$(".accordion").css("color", "#0E1C36");
		$(".panel-profile").css("color", "#13274A");
	}); 

	$("#nightmode").click(function(){	
		$("body").css("background-color", "#13274A");
		$(".accordion").css("background-color", "#234787");
		$(".accordion").css("color", "#F9FBF2");
		$(".panel-profile").css("color", "#3D348B");	
	});

	$("#daymode").hover(function(){
		if($("body").css("background-color") == "rgb(175, 203, 255)") {
			$("body").css("background-color", "#13274A");
			$(".accordion").css("background-color", "#234787");
			$(".accordion").css("color", "#F9FBF2");
			$(".panel-profile").css("color", "#3D348B");
		} else {
			$("body").css("background-color", "#AFCBFF");
			$(".accordion").css("background-color", "#E2ECFF");
			$(".accordion").css("color", "#0E1C36");
			$(".panel-profile").css("color", "#13274A");
		}
	}); 

	$("#nightmode").hover(function(){	
		if($("body").css("background-color") == "rgb(19,39,74)") {
			$("body").css("background-color", "#AFCBFF");
			$(".accordion").css("background-color", "#E2ECFF");
			$(".accordion").css("color", "#0E1C36");
			$(".panel-profile").css("color", "#13274A");
		} else {
			$("body").css("background-color", "#13274A");
			$(".accordion").css("background-color", "#234787");
			$(".accordion").css("color", "#F9FBF2");
			$(".panel-profile").css("color", "#3D348B");
		}
	}); 

	
	var acc = document.getElementsByClassName("accordion");
    var i;
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
  }
});