* Nama				: Khameela Rahmah
* NPM				: 1706023422
* Kelas				: PPW J (2019)
* Link Landing Page	: http://webnyamela.herokuapp.com/landingpage/
* Link Subscribe	: http://webnyamela.herokuapp.com/subscribe/
* Link Wish List	: http://webnyamela.herokuapp.com/wishlist/

## Main Link
http://webnyamela.herokuapp.com/

## Status
[![pipeline status](https://gitlab.com/melarhmh/repo-ppw/badges/master/pipeline.svg)](https://gitlab.com/melarhmh/repo-ppw/commits/master)
[![coverage report](https://gitlab.com/melarhmh/repo-ppw/badges/master/coverage.svg)](https://gitlab.com/melarhmh/repo-ppw/commits/master)