from django.shortcuts import render, redirect
from .forms import StatusForm, SubscribeForm
from .models import Activity, Subscriber
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def landing_page_add_status(request):
    form = StatusForm(request.POST or None)
    response = {}
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        activity = Activity(message = response['message'])
        activity.save()
        return HttpResponseRedirect('/landingpage/')
    else:
        return HttpResponseRedirect('/landingpage/')

def landing_page(request):
    response = {}
    news = Activity.objects.all()
    form = StatusForm()
    return render(request, 'story6_landingpage.html', {'form': form, 'news': news})

def profile_page(request):
    response = {}
    return render(request, 'story6_profile.html', response)

def subscribe(request):
    response = {}
    response["forms"] = SubscribeForm()
    return render(request, 'story10_subscribe.html', response)
    
@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscriber.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)

def success(request):
    submited_form = SubscribeForm(request.POST or None)
    if (submited_form.is_valid()):
        cd = submited_form.cleaned_data
        new_subscriber = Subscriber(name=cd['name'], password=cd['password'], email=cd['email'])
        new_subscriber.save()
        data = {'name': cd['name'], 'password': cd['password'], 'email': cd['email']}
    return JsonResponse(data)
