from django.urls import path
from . import views

app_name ="story6app"

urlpatterns = [
    path('', views.profile_page, name="profile"),
    path('landingpage/', views.landing_page, name="landingpage"),
    path('landingpage_add/', views.landing_page_add_status, name="landingpage_add"),
    path('subscribe/', views.subscribe, name='subscribe'),
    path('validate/', views.validate),
    path('success/', views.success, name='success'),
]