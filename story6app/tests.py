from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .forms import StatusForm, SubscribeForm
from .models import Activity, Subscriber
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.utils.encoding import force_text

# Create your tests here.

class Story6(TestCase):
    def test_story6_using_status_model(self):
        Activity.objects.create(message='Hello')
        jumlahObjek = Activity.objects.all().count()
        self.assertEqual(jumlahObjek, 1)
        
    def test_story6_url_is_exist(self):
        response = Client().get('/landingpage/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/landingpage/')
        self.assertEqual(found.func, landing_page)
        
    def test_story6_add_using_index_func(self):
        found = resolve('/landingpage_add/')
        self.assertEqual(found.func, landing_page_add_status)
    
    def test_story6_using_template(self):
        response = Client().get('/landingpage/')
        self.assertTemplateUsed(response, 'story6_landingpage.html')
    
    def test_story6_form_input_has_placeholder_and_css_classes(self):
        form = StatusForm()
        self.assertIn('class="form-control', form.as_p())

    def test_story6_post_success_and_render_the_result(self):
        test = 'hello'
        response_post = Client().post('/landingpage_add/', {'message': test})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/landingpage/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'hello'
        response_post = Client().post('/landingpage_add/', {'message': ''})
        self.assertEqual(response_post.status_code, 302)
        response = Client().get('/landingpage/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_challenge6_profile_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_challenge6_profile_using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile_page)

    def test_challenge6_profile_has_name(self):
        response = Client().get('/')
        profile_page_content = 'Khameela Rahmah'
        html_response = response.content.decode('utf8')
        self.assertIn(profile_page_content, html_response)

    def test_subscribe_has_url(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_subscribe_using_subscribe_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_subscribe_can_create_object(self):
        Subscriber.objects.create(name="mela", password="hehehehe", email="mela@gmail.com")
        count_all_stats = Subscriber.objects.all().count()
        self.assertEqual(count_all_stats, 1)

    def test_fsubscribe_form_has_placeholder_and_css_classes(self):
        form = SubscribeForm()
        self.assertIn('class="form-control', form.as_p())

    def test_subscribe_form_validation_for_blank_items(self):
        form = SubscribeForm(data={'name': '', 'password': '', 'email':'' })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["Please enter your name"]
        )
        self.assertEqual(
            form.errors['email'],
            ["Please enter a valid email"]
        )
        self.assertEqual(
            form.errors['password'],
            ["This field is required."]
        )

    def test_subscribe_password_less_than_6_char(self):
        form = SubscribeForm(data={'name': 'mela', 'password': 'hehe', 'email':'mela@gmail.com' })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['password'],
            ["password must be longer than 5 characters"]
        )

    def test_double_subscriber(self):
        nama = 'mela'
        password = 'hehehehe'
        email = 'mela@gmail.com'
        go = Client().post('/validate/', {'email': email})
        # must respond false
        self.assertJSONEqual(
            force_text(go.content),
            {'not_valid': False}
        )
        # button not disabled, send data to views
        Client().post('/success/', {'name': nama, 'password': password, 'email': email})
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 1)
        # email used second time
        go = Client().post('/validate/', {'email': email})
        # must respond true, email already taken
        self.assertJSONEqual(
            str(go.content, encoding='utf8'),
            {'not_valid': True}
        )
        # not making any new data
        subscriber_count = Subscriber.objects.all().count()
        self.assertEqual(subscriber_count, 1)

class Story7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://webnyamela.herokuapp.com/landingpage/')
        # find the form element
        form = selenium.find_element_by_class_name('form-control')

        submit = selenium.find_element_by_tag_name('button')

        # Fill the form with data
        form.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        self.assertIn('Coba Coba', selenium.page_source)

    def test_background_color(self):
        selenium = self.selenium
        selenium.get('http://webnyamela.herokuapp.com/landingpage/')
        body = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        self.assertEqual(body, "rgba(175, 203, 255, 1)")

    def test_font(self):
        selenium = self.selenium
        selenium.get('http://webnyamela.herokuapp.com/landingpage/')
        font = selenium.find_element_by_tag_name('h1').value_of_css_property('font-family')
        self.assertIn("Rubik", font)

    def test_location_status_box(self):
        selenium = self.selenium
        selenium.get('http://webnyamela.herokuapp.com/landingpage/')
        locationbox = selenium.find_element_by_class_name('form-control')

    def test_location_submit_button(self):
        selenium = self.selenium
        selenium.get('http://webnyamela.herokuapp.com/landingpage/')
        locationsubmit = selenium.find_element_by_tag_name('button')