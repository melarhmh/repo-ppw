from django import forms

class StatusForm(forms.Form):
	status_attrs = {
	    'type':'text',
	    'class':'form-control',
	    'placeholder':'Your Mood',
	}
	message = forms.CharField(max_length = 800, widget = forms.TextInput(attrs = status_attrs), label = "What's Your Mood?")

class SubscribeForm(forms.Form):
    email_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Example: email@gmail.com',
    }
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Full Name',
    }
    pass_attrs = {
        'type': 'password',
        'class': 'form-control',
        'placeholder':'Password',
    }
    name = forms.CharField(max_length = 50, error_messages={"required": "Please enter your name"}, widget = forms.TextInput(attrs=name_attrs))
    password = forms.CharField(max_length = 20, min_length=6, widget = forms.PasswordInput(attrs=pass_attrs), error_messages={"min_length": "password must be longer than 5 characters"})
    email = forms.EmailField(error_messages={"required": "Please enter a valid email"}, max_length=50, widget=forms.TextInput(attrs=email_attrs))