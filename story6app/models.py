from django.db import models
from django.utils import timezone

# Create your models here.
class Activity(models.Model):
    message = models.CharField(max_length = 800)
    time = models.DateTimeField(auto_now_add=True)

class Subscriber(models.Model):
    name = models.CharField(max_length = 50)
    password = models.CharField(max_length = 20)
    email = models.CharField(max_length = 50)
