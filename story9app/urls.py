from django.urls import path
from . import views

app_name ="story9app"

urlpatterns = [
    path('wishlist/',views.story9_page,name='story9'),
    path('laptop/',views.laptop_page,name='laptop-page'),
]
