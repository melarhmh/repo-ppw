from django.db import models

# Create your models here.
class WishlistLaptop(models.Model):
    name = models.CharField(max_length = 200)
    img = models.CharField(max_length = 200)
    desc = models.CharField(max_length = 200)
    price = models.CharField(max_length=200)