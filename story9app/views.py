from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
import requests

# Create your views here.

def laptop_page(request):
    laptop_url = "https://enterkomputer.com/api/product/notebook.json"
    temp = requests.get(laptop_url).json()
    laptop_json = []
    for items in temp:
        if "Intel" in items['details']:
            laptop_json.append(items)
    laptop_list = []
    count = 0
    for i in range(200,400,1):
        pc_dict = {"id":laptop_json[i]["id"],
        "name":laptop_json[i]["name"],
        "details":laptop_json[i]["details"],
        "brand":laptop_json[i]["brand"], 
        "price":laptop_json[i]["price"]}
        laptop_list.append(pc_dict)
    return JsonResponse({'data':laptop_list})

def story9_page(request):
    response = {}
    return render(request, 'story9_page.html', response)