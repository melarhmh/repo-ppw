from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.test import Client
from .views import story9_page, laptop_page

# Create your tests here.
class Story9Test(TestCase):
    def test_story9_using_index_func(self):
        found = resolve("/wishlist/")
        self.assertEqual(found.func, story9_page)
    
    def test_story9_url_is_exist(self):
        response = Client().get("/wishlist/")
        self.assertEqual(response.status_code, 200)
    
    def test_story9_has_word(self):
        response = Client().get("/wishlist/")
        message = "Wishlist"
        html_response = response.content.decode('utf8')
        self.assertIn(message,html_response)
    
    def test_story9_has_laptop(self):
        response = Client().get("/laptop/")
        lappy = '''{"id": "158936", "name": "Lenovo Ideapad 330-EDID"'''
        html_response = response.content.decode('utf8')
        self.assertIn(lappy,html_response)